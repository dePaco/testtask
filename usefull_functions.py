'''
Usefull functions for diff ML-progects
'''
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def reduce_mem_usage(df):
    """ 
    iterate through all the columns of a dataframe and 
    modify the data type to reduce memory usage.

    df --> pandas dataframe

    Requirements: 
    import numpy as np        
    """
    start_mem = df.memory_usage().sum() / 1024**2
    print(('Memory usage of dataframe is {:.2f}' 
                     'MB').format(start_mem))
    
    for col in df.columns:
        col_type = df[col].dtype
        
        if col_type != object:
            c_min = df[col].min()
            c_max = df[col].max()
            if str(col_type)[:3] == 'int':
                if c_min > np.iinfo(np.int8).min and c_max <\
                  np.iinfo(np.int8).max:
                    df[col] = df[col].astype(np.int8)
                elif c_min > np.iinfo(np.int16).min and c_max <\
                   np.iinfo(np.int16).max:
                    df[col] = df[col].astype(np.int16)
                elif c_min > np.iinfo(np.int32).min and c_max <\
                   np.iinfo(np.int32).max:
                    df[col] = df[col].astype(np.int32)
                elif c_min > np.iinfo(np.int64).min and c_max <\
                   np.iinfo(np.int64).max:
                    df[col] = df[col].astype(np.int64)  
            else:
                if c_min > np.finfo(np.float16).min and c_max <\
                   np.finfo(np.float16).max:
                    df[col] = df[col].astype(np.float16)
                elif c_min > np.finfo(np.float32).min and c_max <\
                   np.finfo(np.float32).max:
                    df[col] = df[col].astype(np.float32)
                else:
                    df[col] = df[col].astype(np.float64)
        else:
            df[col] = df[col].astype('category')
    end_mem = df.memory_usage().sum() / 1024**2
    print(('Memory usage after optimization is: {:.2f}' 
                              'MB').format(end_mem))
    print('Decreased by {:.1f}%'.format(100 * (start_mem - end_mem) 
                                             / start_mem))
    
    return df


def draw_covar(tmp):
    """Матрица ковариаций
    """
    matrix_cov = tmp.cov()
    plt.figure(figsize=(10, 10))
    sns.heatmap(matrix_cov)
    plt.title('Тепловая карта матрицы ковариаций')
    
    return matrix_cov


def draw_pearson(tmp_clear, power_corr = 0.5):
    """Корреляция Пирсона - мера силы линейной корреляции между признаками
    """
    matrix_corr_pearson = tmp_clear.corr(method='pearson')
    plt.figure(figsize=(10, 10))
    cmap = sns.diverging_palette(230, 20, as_cmap=True)
    sns.heatmap(matrix_corr_pearson, annot=True, linewidths=.5, cmap=cmap, vmax=.3, center=0, square=True)
    plt.title('Тепловая карта корреляции Пирсона')
    
    plt.figure(figsize=(10, 10))
    cmap = sns.diverging_palette(230, 20, as_cmap=True)
    sns.heatmap(matrix_corr_pearson[(matrix_corr_pearson.iloc[:,:] < -power_corr) 
                                    & (matrix_corr_pearson.iloc[:,:] > -1.0) | 
                                    (matrix_corr_pearson.iloc[:,:] > power_corr) 
                                    & (matrix_corr_pearson.iloc[:,:] < 1.0)]
                                    , annot=True, linewidths=.5, cmap=cmap, vmax=.3, center=0, square=True)
    plt.title('Тепловая карта корреляции Пирсона от {} по модулю'.format(power_corr))
    
    return matrix_corr_pearson


def draw_spearmen(tmp_clear, power_corr = 0.5):
    """Корреляция Спирмена - мера силы монотонной корреляции между признаками
    (равен корреляции Пирсона между рангами наблюдений)
    """
    matrix_corr_spearman = tmp_clear.corr(method='spearman')
    plt.figure(figsize=(10, 10))
    cmap = sns.diverging_palette(230, 20, as_cmap=True)
    sns.heatmap(matrix_corr_spearman, annot=True, linewidths=.5, cmap=cmap, vmax=.3, center=0, square=True)
    plt.title('Тепловая карта корреляции Спирмена')
    
    plt.figure(figsize=(10, 10))
    cmap = sns.diverging_palette(230, 20, as_cmap=True)
    sns.heatmap(matrix_corr_spearman[(matrix_corr_spearman.iloc[:, :] > -1.0)
                                     & (matrix_corr_spearman.iloc[:, :] < -power_corr) | 
                                     (matrix_corr_spearman.iloc[:, :] > power_corr)
                                     & (matrix_corr_spearman.iloc[:, :] < 1.0)]
                                     , annot=True, linewidths=.5, cmap=cmap, vmax=.3, center=0, square=True)
    plt.title('Тепловая карта корреляции Спирмена от {} по модулю'.format(power_corr))
    
    return matrix_corr_spearman


def draw_kendall(tmp_clear, power_corr = 0.5):
    """Корреляция Кендалла - мера взаимной неупорядоченности признаков.
    разность вероятностей совпадения и инверсии в рангах.
    """
    matrix_corr_kendall = tmp_clear.corr(method='kendall')
    plt.figure(figsize=(10, 10))
    cmap = sns.diverging_palette(230, 20, as_cmap=True)
    sns.heatmap(matrix_corr_kendall, annot=True, linewidths=.5, cmap=cmap, vmax=.3, center=0, square=True)
    plt.title('Тепловая карта корреляции Кендалла')
    
    plt.figure(figsize=(10, 10))
    cmap = sns.diverging_palette(230, 20, as_cmap=True)
    sns.heatmap(matrix_corr_kendall[(matrix_corr_kendall.iloc[:, :] > -1.0)
                                     & (matrix_corr_kendall.iloc[:, :] < -power_corr) | 
                                     (matrix_corr_kendall.iloc[:, :] > power_corr)
                                     & (matrix_corr_kendall.iloc[:, :] < 1.0)]
                                     , annot=True, linewidths=.5, cmap=cmap, vmax=.3, center=0, square=True)
    plt.title('Тепловая карта корреляции Кендалла от {} по модулю'.format(power_corr))
    
    return matrix_corr_kendall


def get_analis(data):
    """Получаем статистики и графики по признакам и их корреляциям
    """
    stat_dict = {}
    moda = {}
    var = {}
    summ = {}
    uniq = {}
    
    for a in data.columns:
        plt.figure(figsize=(10, 10))
        sns.distplot(data[a], kde = False)
        plt.title('Распределение признака ' + str(a))
    
    for a in data.columns:
        stat_dict[a] = data[a].describe()
        moda[a] = data[a].mode()
        var[a] = data[a].var()
        summ[a] = data[a].sum()
        uniq[a] = data[a].unique()
 
    stats_df = pd.DataFrame(stat_dict)
    
    raz = stats_df.loc['max', :] - stats_df.loc['min', :]
    
    draw_covar(data)
    draw_pearson(data)
    draw_spearmen(data)
    draw_kendall(data)
       
    return {'stats': stats_df, 
     'moda': moda, 
     'var': var, 
     'sum': summ,  
     'uniq': uniq, 
     'raz': raz}


def median_filter(df, varname = None, window=24, std=3): 
    """
    A simple median filter, removes (i.e. replace by np.nan) observations that exceed N (default = 3) 
    tandard deviation from the median over window of length P (default = 24) centered around 
    each observation.
    Parameters
    ----------
    df : pandas.DataFrame
        The pandas.DataFrame containing the column to filter.
    varname : string
        Column to filter in the pandas.DataFrame. No default. 
    window : integer 
        Size of the window around each observation for the calculation 
        of the median and std. Default is 24 (time-steps).
    std : integer 
        Threshold for the number of std around the median to replace 
        by `np.nan`. Default is 3 (greater / less or equal).
    Returns
    -------
    dfc : pandas.Dataframe
        A copy of the pandas.DataFrame `df` with the new, filtered column `varname`
    """
    
    dfc = df.loc[:,[varname]]
    dfc['median']= dfc[varname].rolling(window, center=True).median()
    dfc['std'] = dfc[varname].rolling(window, center=True).std()
    dfc.loc[dfc.loc[:,varname] >= dfc['median']+std*dfc['std'], varname] = np.nan
    dfc.loc[dfc.loc[:,varname] <= dfc['median']-std*dfc['std'], varname] = np.nan
    
    return dfc.loc[:, varname]