DATA:
=========
- calendar.csv - Contains information about the dates on which the products are sold.
- sales_train_validation.csv - Contains the historical daily unit sales data per product and store [d_1 - d_1913]
- sample_submission.csv - The correct format for submissions. Reference the Evaluation tab for more info.
- sell_prices.csv - Contains information about the price of the products sold per store and date.
- sales_train_evaluation.csv - Includes sales [d_1 - d_1941] (labels used for the Public leaderboard

KEY POINTS:
==========
- hierarchical sales data from Walmart
- forecast daily sales for the next 28 days
- The data, covers stores in three US States (California, Texas, and Wisconsin) and includes item level, department, product categories, and store details. In addition, it has explanatory variables such as price, promotions, day of the week, and special events.

> **This competition uses a Weighted Root Mean Squared Scaled Error (RMSSE)**

LINKS:
======
https://mofc.unic.ac.cy/m5-competition/ 
https://www.kaggle.com/competitions/m5-forecasting-accuracy/
